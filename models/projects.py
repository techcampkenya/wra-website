from configs.dbConfig import db

class Projects(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    file_url = db.Column(db.String(),nullable=False)
    descriptions = db.Column(db.String(),nullable=False)
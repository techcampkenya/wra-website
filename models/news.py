from sqlalchemy import DateTime, func
from configs.dbConfig import db
class News(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    description=db.Column(db.String(),nullable=True)
    fileurl=db.Column(db.String(),nullable=True)
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=False)
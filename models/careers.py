from sqlalchemy import DateTime, func
from configs.dbConfig import db
class Careers(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(100))
    career_description = db.Column(db.String(),nullable=False)
    career_fileurl=db.Column(db.String(),nullable=True)
    expiry_date=db.Column(db.String(),nullable=True)
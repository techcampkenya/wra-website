from sqlalchemy import DateTime, func
from configs.dbConfig import db
class Licence(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    ref_number= db.Column(db.String())
    id_number = db.Column(db.String(),nullable=False)
    email=db.Column(db.String(),nullable=True)
    status=db.Column(db.String(),nullable=False)
from configs.dbConfig import db

class Charter(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    english_pdf= db.Column(db.String(),nullable=True)
    kiswahili_pdf= db.Column(db.String(),nullable=True)
    english_audio= db.Column(db.String(),nullable=True)
    kiswahili_audio= db.Column(db.String(),nullable=True)

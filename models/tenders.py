from configs.dbConfig import db

class Tenders(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    ref_number = db.Column(db.String(),unique=True,nullable=False)
    title = db.Column(db.String(),nullable=False)
    department = db.Column(db.String(),nullable=False)
    upload_date = db.Column(db.String(),nullable=False)
    due_date= db.Column(db.String(),nullable=False)
    file_url = db.Column(db.String(),nullable=True)
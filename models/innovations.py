from email.mime import image
from configs.dbConfig import db
from sqlalchemy import DateTime, func
class Innovation(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=True)
    file_url = db.Column(db.String(),nullable=True)
    pdf_url= db.Column(db.String(),nullable=True)
    coverpage_description= db.Column(db.String(),nullable=True)
    descriptions = db.Column(db.String(),nullable=True)
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=True)
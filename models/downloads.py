from configs.dbConfig import db
from sqlalchemy import DateTime, func

class Downloads_model(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    category = db.Column(db.String(),nullable=False)
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=False)
    file_url = db.Column(db.String(),nullable=False)

    
from configs.dbConfig import db
from flask_login import UserMixin



class Users(UserMixin,db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True) 
    email = db.Column(db.String(100),nullable=False, unique=True)
    password = db.Column(db.String(100))
    username = db.Column(db.String(1000))
    role = db.Column(db.String(1000))
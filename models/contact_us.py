from configs.dbConfig import db

class Contact_model(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    full_name = db.Column(db.String(),nullable=True)
    email = db.Column(db.String(),nullable=False, unique=True)
    message = db.Column(db.String(),nullable=True)


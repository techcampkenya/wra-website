
from unicodedata import category, name
from sqlalchemy import DateTime, func
from configs.dbConfig import db
class Boardmembers(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(),nullable=False)
    position=db.Column(db.String(),nullable=False)
    description = db.Column(db.String(),nullable=True)
    fileurl=db.Column(db.String(),nullable=True)
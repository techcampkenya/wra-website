from sqlalchemy import DateTime, func
from configs.dbConfig import db

class Category_model(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    category_name = db.Column(db.String(),nullable=False)
    category_route_name = db.Column(db.String(),nullable=False) #NEW ADDED FIELD
    category = db.relationship('Item_model', backref=db.backref('items', lazy=True),passive_deletes='all')
    

class Item_model(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    cover_page_title = db.Column(db.String(),nullable=True)
    cover_page_description = db.Column(db.String(),nullable=True)
    body_image = db.Column(db.String(),nullable=True)
    cover_image=db.Column(db.String(),nullable=True)
    pdf=db.Column(db.String(),nullable=True)
    body_text = db.Column(db.String(),nullable=False)
    # cover_page_image = db.Column(db.String(),nullable=False)
    item_name = db.Column(db.String(),nullable=True)
    sort_order = db.Column(db.Integer(),default=0) #NEW ADDED FIELD
    item_route_name = db.Column(db.String(),nullable=True) #NEW ADDED FIELD
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('category_model.id'))

class License_model(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(),nullable=False)
    message = db.Column(db.String(),nullable=False)


from configs.dbConfig import db

class Offices(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    image_url = db.Column(db.String(),nullable=False)
    descriptions = db.Column(db.String(),nullable=False)
    pdf_url = db.Column(db.String(),nullable=False)
    cover_image_url = db.Column(db.String(),nullable=True)
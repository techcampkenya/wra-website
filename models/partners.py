from configs.dbConfig import db
from sqlalchemy import DateTime, func

class Partners(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    file_url = db.Column(db.String(),nullable=False)
    link=db.Column(db.String(),nullable=True)
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=False)
    
    
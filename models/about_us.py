from configs.dbConfig import db
from sqlalchemy import DateTime, func

class About(db.Model):

    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(),nullable=False)
    file_url = db.Column(db.String(),nullable=False)
    descriptions = db.Column(db.String(),nullable=False)
    created_at = db.Column(DateTime(timezone=True), default=func.now(), nullable=False)
    sort_order = db.Column(db.Integer(),default=0)
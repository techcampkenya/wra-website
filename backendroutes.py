from re import I
from sndhdr import whathdr
from flask import render_template,request,redirect,url_for,flash
from flask_login import LoginManager,current_user, login_user,logout_user
from frontendroutes import what_we_do_generic
from models.about_us import About
from models.contact_us import Contact_model
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from flask.helpers import url_for
from main import app
from configs.dbConfig import TestingConfig,db
from models.category import Category_model, License_model
import os
from werkzeug.utils import secure_filename
from models.downloads import Downloads_model
from models.gallery import Gallery_model
from models.category import Category_model,Item_model
from models.users import *
from models.tenders import Tenders
from models.careers import Careers
from models.boardmembers import Boardmembers
from models.news import News
from models.licence import Licence
from models.projects import Projects
from models.regionaloffices import Offices
from models.partners import Partners
from models.seniormanagement import Seniormanagement
from models.partners import Partners
from models.about_us import About
from models.charter import Charter
from models.index import Index
from models.innovations import Innovation

baseUrl = '/admin'
GALLERY_UPLOAD_FOLDER = 'static/images/gallery_pictures'
ABOUTUS_UPLOAD_FOLDER = 'static/images/aboutus_pictures'
PARTNERS_UPLOAD_FOLDER = 'static/images/partners_logo'
WHAT_WE_DO_PICTURES_FOLDER = 'static/images/what_we_do_pictures'
WHAT_WE_DO_PDFS_FOLDER = 'static/PDFS/what_we_do_pdfs'
NEWS_IMAGES_FOLDER = 'static/images/news_images'
NEWS_PDFS_FOLDER = 'static/PDFS/news_pdfs'
CAREER_FOLDER='static/PDFS/careers_pdfs'
BOARD_MEMBERS_PICTURES_FOLDER = 'static/images/boardmembers_pictures'
SENIOR_MANAGEMENT_PICTURES_FOLDER = 'static/images/seniormanagement_pictures'
PUBLICATIONS_FOLDER = 'static/PDFS/publications_and_downloads_pdfs'
TENDERS_PDFS_FOLDER = 'static/PDFS/tenders_pdfs'
PROJECT_PICTURES_FOLDER = 'static/images/projects_images'
REGIONAL_IMAGES_FOLDER = 'static/images/regionaloffices_images'
REGIONAL_PDFS_FOLDER = 'static/PDFS/regionaloffices_pdfs'
CHARTER_AUDIO_FOLDER = 'static/assets/audio'
CHARTER_PDF_FOLDER = 'static/assets/pdfs'
UPLOAD_FOLDER = 'static/userImageUpload'
PDF_UPLOAD_FOLDER = 'static/userImageUpload'
INNOVATION_PICTURES_FOLDER = 'static/images/innovations_images'
INNOVATION_PDFS_FOLDER = 'static/PDFS/innovations_pdfs'




PICTURE_ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif','jfif'}
PDF_ALLOWED_EXTENSIONS = {'pdf','docx'}
AUDIO_ALLOWED_EXTENSIONS = {'mp3','wav','ogg','mp4','mov'}

app.config.from_object(TestingConfig)
app.config['CHARTER_AUDIO_FOLDER']= CHARTER_AUDIO_FOLDER
app.config['CHARTER_PDF_FOLDER']= CHARTER_PDF_FOLDER
app.config['ABOUTUS_PICTURES'] =ABOUTUS_UPLOAD_FOLDER
app.config['GALLERY_PICTURES'] = GALLERY_UPLOAD_FOLDER
app.config['PARTNERS_LOGO'] = PARTNERS_UPLOAD_FOLDER
app.config['WHAT_WE_DO_PICTURES'] = WHAT_WE_DO_PICTURES_FOLDER
app.config['WHAT_WE_DO_PDFS'] = WHAT_WE_DO_PDFS_FOLDER
app.config['NEWS_PICTURES'] = NEWS_IMAGES_FOLDER
app.config['NEWS_PDF'] = NEWS_PDFS_FOLDER
app.config['BOARD_MEMBERS_PICTURES_FOLDER'] = BOARD_MEMBERS_PICTURES_FOLDER
app.config['SENIOR_MANAGEMENT_PICTURES_FOLDER'] = SENIOR_MANAGEMENT_PICTURES_FOLDER
app.config['PUBLICATIONS_FOLDER'] = PUBLICATIONS_FOLDER
app.config['TENDERS_PDFS_FOLDER'] = TENDERS_PDFS_FOLDER
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['PDF_UPLOAD_FOLDER'] = PDF_UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 20000 * 1024 * 1024
app.config['CAREER_FOLDER'] = CAREER_FOLDER
app.config['PROJECT_PICTURES_FOLDER'] = PROJECT_PICTURES_FOLDER
app.config['REGIONALOFFICES_PICTURES'] = REGIONAL_IMAGES_FOLDER
app.config['REGIONALOFFICES_PDF'] = REGIONAL_PDFS_FOLDER
app.config['INNOVATIONS_PICTURES_FOLDER'] = INNOVATION_PICTURES_FOLDER 
app.config['INNOVATIONS_PDF_FOLDER'] = INNOVATION_PDFS_FOLDER 


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in PICTURE_ALLOWED_EXTENSIONS
def allowed_file_pdf(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in PDF_ALLOWED_EXTENSIONS
def allowed_file_audio(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in AUDIO_ALLOWED_EXTENSIONS

def charter_audio(file):
    if file and allowed_file_audio(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['CHARTER_AUDIO_FOLDER'], filename))
        audio1=os.path.join(app.config['CHARTER_AUDIO_FOLDER'],filename)
        audio1 = audio1.replace("\\", "/")
        print("care about code", audio1)
        return audio1 
def charter_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['CHARTER_PDF_FOLDER'], filename))
        pdf_file=os.path.join(app.config['CHARTER_PDF_FOLDER'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)
        return pdf_file
def partners_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['PARTNERS_LOGO'], filename))
        cover_image=os.path.join(app.config['PARTNERS_LOGO'],filename)
        partner_picture = cover_image.replace("\\", "/")
        print("care about code", partner_picture)
        return partner_picture 
def gallery_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['GALLERY_PICTURES'], filename))
        cover_image=os.path.join(app.config['GALLERY_PICTURES'],filename)
        gallery_picture = cover_image.replace("\\", "/")
        print("care about code", gallery_picture)
        return gallery_picture 
def about_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['ABOUTUS_PICTURES'], filename))
        cover_image=os.path.join(app.config['ABOUTUS_PICTURES'],filename)
        about_picture = cover_image.replace("\\", "/")
        print("care about code", about_picture)
        return about_picture 
def project_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['PROJECT_PICTURES_FOLDER'], filename))
        cover_image=os.path.join(app.config['PROJECT_PICTURES_FOLDER'],filename)
        project_picture = cover_image.replace("\\", "/")
        return project_picture  
def whatwedo_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['WHAT_WE_DO_PICTURES'], filename))
        cover_image=os.path.join(app.config['WHAT_WE_DO_PICTURES'],filename)
        whatwedopicture = cover_image.replace("\\", "/")
        print("care about code", whatwedopicture)
        return whatwedopicture

def whatwe_do_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['WHAT_WE_DO_PDFS'], filename))
        pdf_file=os.path.join(app.config['WHAT_WE_DO_PDFS'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file
def news_uploads(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['NEWS_PICTURES'], filename))
        cover_image=os.path.join(app.config['NEWS_PICTURES'],filename)
        news_image = cover_image.replace("\\", "/")
        print("care about code", news_image)
        return news_image

def news_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['NEWS_PDF'], filename))
        pdf_file=os.path.join(app.config['NEWS_PDF'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file  
def regionaloffices_uploads(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['REGIONALOFFICES_PICTURES'], filename))
        cover_image=os.path.join(app.config['REGIONALOFFICES_PICTURES'],filename)
        regionaloffice_image = cover_image.replace("\\", "/")
        print("care about code", regionaloffice_image)
        return regionaloffice_image  

def regionaloffices_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['REGIONALOFFICES_PDF'], filename))
        pdf_file=os.path.join(app.config['REGIONALOFFICES_PDF'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file 

def upload_career_file(file):
    if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['CAREER_FOLDER'], filename))
        career_file=os.path.join(app.config['CAREER_FOLDER'],filename)
        print('=' *100)
        print(career_file)
        return career_file

def board_members_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['BOARD_MEMBERS_PICTURES_FOLDER'], filename))
        cover_image=os.path.join(app.config['BOARD_MEMBERS_PICTURES_FOLDER'],filename)
        board_members_image = cover_image.replace("\\", "/")
        print("care about code", board_members_image)
        return board_members_image
def seniormanagement_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['SENIOR_MANAGEMENT_PICTURES_FOLDER'], filename))
        cover_image=os.path.join(app.config['SENIOR_MANAGEMENT_PICTURES_FOLDER'],filename)
        senior_management_image = cover_image.replace("\\", "/")
        
        return senior_management_image
def publications_uploads(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['PUBLICATIONS_FOLDER'], filename))
        pdf_file=os.path.join(app.config['PUBLICATIONS_FOLDER'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file

def tenders_uploads(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['TENDERS_PDFS_FOLDER'], filename))
        pdf_file=os.path.join(app.config['TENDERS_PDFS_FOLDER'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file 



def upload_picture(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        cover_image=os.path.join(app.config['UPLOAD_FOLDER'],filename)
        new_cover_image = cover_image.replace("\\", "/")
        print("care about code", new_cover_image)
        return new_cover_image

def upload_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        pdf_file=os.path.join(app.config['UPLOAD_FOLDER'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)

        print('=' *100)
        print(pdf_file)
        return pdf_file

def innovation_upload(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['INNOVATIONS_PICTURES_FOLDER'], filename))
        cover_image=os.path.join(app.config['INNOVATIONS_PICTURES_FOLDER'],filename)
        innovation_picture = cover_image.replace("\\", "/")
        return innovation_picture  

def innovation_pdf(file):
   if file and allowed_file_pdf(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['INNOVATIONS_PDF_FOLDER'], filename))
        pdf_file=os.path.join(app.config['INNOVATIONS_PDF_FOLDER'],filename)
        pdf_file = pdf_file.replace("\\", "/")
        print("care about pdf", pdf_file)
        return pdf_file
@app.before_first_request
def create():
    
    db.create_all()

    login_manager = LoginManager()
    login_manager.login_view = 'login'
    login_manager.init_app(app)
    
    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return Users.query.get(int(user_id))



def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not current_user.is_authenticated :
            flash('Login first to access this page.')
            return redirect('/admin/login')
        else:        
            
            return  f(*args, **kwargs)
    return wrapper

   
@app.route(f'{baseUrl}/users')
@login_required
def admin():
    records =Users.query.all()
    
    return render_template('backend/admin.html',myUsers=records)


@app.route(f'{baseUrl}/login')
def login():
    return render_template('backend/login.html')


@app.route(f'{baseUrl}/register')
def register():
    return render_template('backend/register.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/admin')


@app.route(f'{baseUrl}/register', methods=['GET','POST'])
def signup_post():
  
    email = request.form.get('email')
    name = request.form.get('username')
    password = request.form.get('password')
    role = request.form.get('role')
   
    user = Users.query.filter_by(email=email).first()

    if user:
        flash('Email address already exists')
        return redirect(url_for('register'))

    new_user = Users(email=email, username=name, password=password,role=role)

    db.session.add(new_user)
    db.session.commit()
    
    login_user(new_user)

    return redirect('/admin/users')


@app.route(f'{baseUrl}/login', methods=['GET','POST'])
def login_post():
   
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = Users.query.filter_by(email=email).first()
  
    if not user or  user.password!=password :
        flash('Please check your login details and try again.')
        return redirect(url_for('login'))
    
    login_user(user, remember=remember)
    
    return redirect('/admin')


@app.route('/create_user', methods=['POST'])
@login_required
def create_user():
    email = request.form.get('email')
    username =request.form['username']
    role= request.form['role']
    password = request.form['password']
    user = current_user.id
    rows = Users(username=username,role=role,password=password,email=email)
    db.session.add(rows)
    db.session.commit()
   
    return redirect('/admin/users')

@app.route('/delete_user',methods=['post','get'])
@login_required
def delete_user():
    id = request.form['user_id']
    row = Users.query.filter_by(id = id).one()
     
    db.session.delete(row)
    db.session.commit()
    return redirect('/admin/users')


@app.route(f"{baseUrl}/upload-downloads", methods=['GET', 'POST'])
@login_required
def upload_downloads():
    if request.method == 'POST':
        title = request.form['title']
        category = request.form['category']

        if 'pdf_url' not in request.files:
            flash('No file part')
            
        file = request.files['pdf_url']
        if file.filename == '':
            flash('No selected file')
            
        pub_file=publications_uploads(file)
        new_publication= Downloads_model(title=title,category=category,file_url=pub_file)
        db.session.add(new_publication)
        db.session.commit()
        flash('Item added successfully')
        return redirect(url_for('upload_downloads'))
        
            

    allDownloads = Downloads_model.query.all()
    
    return render_template("backend/upload-download.html",allDownloads=allDownloads)

@app.route(f"{baseUrl}/delete-downloads/<int:id>", methods=['GET','POST'])
@login_required
def de_download(id):

    Downloads_model.query.filter_by(id=id).delete()
    # download.delete()
    db.session.commit()
    return redirect(url_for('upload_downloads'))

@app.route(f"{baseUrl}/edit-downloads/<int:id>", methods=['POST','GET'])
@login_required
def edit_downloads(id):
    if request.method == 'POST':
        download = Downloads_model.query.filter_by(id=id).first()
        title = request.form['title']
       
        category = request.form['category']
        
        file = request.files['pdf_url']
        if file.filename == '':
            download.file_url = download.file_url
        else:
            pub_file=publications_uploads(file)
            download.file_url = pub_file
    

        download.title = title
        download.category = category
        db.session.add(download)
        db.session.commit()
        return redirect(url_for('upload_downloads'))
    return redirect(url_for('upload_downloads'))


@app.route(f"{baseUrl}/delete-gallery/<int:id>", methods=['GET','POST'])
@login_required
def delete_gallery(id):
    gallery = Gallery_model.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('upload_gallery'))


@app.route(f"{baseUrl}/edit-gallery/<int:id>", methods=['POST','GET'])
@login_required
def edit_gallery(id):
    if request.method == 'POST':
        gallery = Gallery_model.query.filter_by(id=id).first()
        title = request.form['title']
        url = request.form['url']
        if len(url)!=0:
            gallery.file_url = url
            gallery.title = title
            db.session.commit()
        gallery.title = title
        db.session.commit()
    
        return redirect(url_for('upload_gallery'))
    return redirect(url_for('upload_gallery'))


# Category and Item
@app.route(f"{baseUrl}/category", methods=['POST','GET'])
@login_required
def admin_category():
    categories = Category_model.query.all()
    if request.method == 'POST':
        category_name = request.form['category']
        category_route_name = request.form['category_route_name'] #NEW ADDED FIELD
        category = Category_model(category_name=category_name,category_route_name=category_route_name)
        db.session.add(category)
        db.session.commit()
        flash('Category added')
        return redirect(url_for('admin_category'))

    return render_template("backend/category.html",categories=categories)


@app.route(f"{baseUrl}/edit/category/<int:id>", methods=['POST'])
@login_required
def admin_edit_category(id):
    if request.method == 'POST':
        category = Category_model.query.filter_by(id=id).first()
        category_name = request.form['category_name']
        category_route_name = request.form['category_route_name'] #NEW ADDED FIELD
        if len(category_name)!=0:
            category.category_name = category_name
            category.category_route_name = category_route_name  #NEW ADDED FIELD
            db.session.add(category)
            db.session.commit()
            flash('Category updated')
            return redirect(url_for('admin_category'))


@app.route(f"{baseUrl}/delete/category/<int:id>", methods=['POST'])
@login_required
def admin_delete_category(id):
    if request.method == 'POST':
        cat=Item_model.query.filter_by(category_id=id).all()
        if len(cat)==0:
            Category_model.query.filter_by(id=id).delete()
            db.session.commit()
            flash('Category deleted')
            return redirect(url_for('admin_category'))
        else:
            flash('You can not delete this category')
            return redirect(url_for('admin_category'))
        # Category_model.query.filter_by(id=id).delete()
        # db.session.commit()
   
        # return redirect(url_for('admin_category'))
@app.route(f"{baseUrl}/newscategory/<int:id>/items", methods=['POST','GET'])
@login_required
def admin_news_item(id):
    items = Item_model.query.filter_by(category_id=id).all()
    category_name = Category_model.query.filter_by(id=id).first().category_name
    category_route_name = Category_model.query.filter_by(id=id).first().category_route_name
    if request.method == 'POST':
        title = request.form['title']
        description = request.form['description']
        item_route_name = request.form['item_route_name']
        if 'news_image' not in request.files:
            flash('No file part')
            
        file = request.files['news_image']
        if file.filename == '':
            flash('No selected file')
            
        news_image=news_uploads(file)
        
        print(news_image)
        if news_image==None:
           news_image=''
        if 'pdf' not in request.files:
            flash('No file part')
            print('No file part')
            
        file = request.files['pdf']
        if file.filename == '':
            
            print('No pdf file part')
        
        pdf_file=news_pdf(file)
        print("pdf")
        print(pdf_file)
        data = Item_model(category_id=id, cover_page_title=title,item_route_name=item_route_name,cover_page_description=description,body_image=news_image,sort_order="none",body_text="none",item_name="none",pdf=pdf_file)
        db.session.add(data)
        db.session.commit()
        flash('News Submitted')
        return redirect(url_for('admin_item',id=id,category_name=category_name,category_route_name=category_route_name))



@app.route(f"{baseUrl}/category/<int:id>/items", methods=['POST','GET'])
@login_required
def admin_item(id):
    items = Item_model.query.filter_by(category_id=id).all()
    category_name = Category_model.query.filter_by(id=id).first().category_name
    category_route_name = Category_model.query.filter_by(id=id).first().category_route_name

    if request.method == 'POST':

        cover_page_title = request.form['cover_page_title']
        item_name = request.form['item_name']
        sort_order = request.form['sort_order'] #NEW ADDED FIELD
        item_route_name = request.form['item_route_name'] #NEW ADDED FIELD
        cover_page_description = request.form['cover_page_description']
        body_text = request.form['body_text']
       

        if 'body_image' not in request.files:
            flash('No file part')
            
            
        file = request.files['body_image']
        if file.filename == '':
            flash('No selected file')
            
        body_image=whatwedo_upload(file)
        print("body_image")
        data = Item_model(category_id=id, cover_page_title=cover_page_title,sort_order=sort_order,item_route_name=item_route_name,cover_page_description=cover_page_description,body_text=body_text,body_image=body_image, item_name=item_name)
        db.session.add(data)
        db.session.commit()
        flash('New What We Do Item added')
            
                


        if 'cover_image' not in request.files:
            flash('No file part')
            print('No file part')
            
        file = request.files['cover_image']
        if file.filename == '':
            flash('No selected file')
            print('No file part')
        new_cover_image=whatwedo_upload(file)
        print("new_cover_image")
        cover_id=Item_model.query.order_by(Item_model.id.desc()).first()
        cover_id=cover_id.id
        print(id)
        saved_cover_image = Item_model.query.filter_by(id=cover_id).update({'cover_image':new_cover_image})
        db.session.commit()
        


        if 'pdf' not in request.files:
            flash('No file part')
            print('No file part')
            
        file = request.files['pdf']
        if file.filename == '':
            flash('No selected file')
            print('No pdf file part')
        
        pdf_file=whatwe_do_pdf(file)
        print("pdf")
        pdf_id=Item_model.query.order_by(Item_model.id.desc()).first()
        pdf_id=pdf_id.id
        print(id)
        pdf= Item_model.query.filter_by(id=pdf_id).update({'pdf':pdf_file})
        db.session.commit()
        return redirect(url_for('admin_item',id=id))
        
    if category_route_name=='wra-news':
        news=0
        print("news")
    else:
        news=1
    return render_template("backend/upload-item.html",items=items,category_id=id,category_name=category_name,news=news)

@app.route(f"{baseUrl}/delete/category/<int:categoryID>/item/<int:id>", methods=['POST','GET'])
@login_required
def admin_delete_item(id,categoryID):
    Item_model.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Item Deleted')

    return redirect(url_for('admin_item', id=categoryID))


@app.route(f"{baseUrl}/contact-us")
@login_required
def admin_contact():
    cont = Contact_model.query.all()
   
    return render_template("backend/contact.html", cont = cont)
    
@app.route(f"{baseUrl}/licenses")
@login_required
def admin_licenses():
    items=Licence.query.all()
    return render_template("backend/license_view.html",items=items)

@app.route(f"{baseUrl}/upload-gallery", methods=['POST','GET'])
@login_required
def gallery_uploads():
    
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        if 'gallery_image' not in request.files:
            flash('No file part')
            return redirect(url_for('gallery_uploads'))
        file = request.files['gallery_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('gallery_uploads'))
        gallery_image=gallery_upload(file)
        data = Gallery_model(file_url=gallery_image, title=title,descriptions=description)
        db.session.add(data)
        db.session.commit()
        flash('Gallery Image Uploaded')
        return redirect(url_for('gallery_uploads'))
    items=Gallery_model.query.all()
    return render_template("backend/galleryupload.html",items=items)
@app.route("/gallery/delete/<int:id>", methods=['POST','GET'])
@login_required
def gallery_delete(id):
    Gallery_model.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Gallery Image Deleted')

    return redirect(url_for('gallery_uploads'))
@app.route("/tender_upload", methods=['POST','GET'])
@login_required
def tender_upload():
    if request.method == 'POST':
        ref_number=request.form['ref']
        title=request.form['title']
        department=request.form['department']
        
        upload_date=request.form['uploaddate']
        due_date=request.form['duedate']
        file=request.files['pdf_url']
        pdf_url=tenders_uploads(file)
        data=Tenders(ref_number=ref_number,title=title,department=department,upload_date=upload_date,due_date=due_date,file_url=pdf_url)
        db.session.add(data)
        db.session.commit()
        return redirect(url_for('tender_upload'))
    items=Tenders.query.all()
    return render_template("backend/tenders_upload.html",items=items)
@app.route("/tender/delete/<int:id>", methods=['POST','GET'])
@login_required
def tender_delete(id):
    Tenders.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    return redirect(url_for('tender_upload'))
@app.route("/admin/careerupload", methods=['POST','GET'])
@login_required
def career_upload():
    if request.method == 'POST':
        title=request.form['career_title']
        career_description=request.form['career_description']
        expiry_date=request.form['duedate']
        
        if 'career_pdf' not in request.files:
            flash('No file part')
            
        file = request.files['career_pdf']
        if file.filename == '':
            flash('No selected file')
            
        career_pdf=upload_career_file(file)
        data = Careers(career_description=career_description,career_fileurl=career_pdf,expiry_date=expiry_date,title=title)
        db.session.add(data)
        db.session.commit()
        flash('New Career Uploaded')
        return redirect(url_for('career_upload'))
    items=Careers.query.all()
    return render_template("backend/career_upload.html",items=items)
@app.route("/career/delete/<int:id>", methods=['POST','GET'])
@login_required
def career_delete(id):
    Careers.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    return redirect(url_for('career_upload'))
@app.route("/admin/addboardmember", methods=['POST','GET'])
@login_required
def board_member_upload():
    if request.method == 'POST':
        name=request.form['name']
        position=request.form['position']
        
        if 'board_image' not in request.files:
            flash('No file part')
            return redirect(url_for('board_member_upload'))
        file = request.files['board_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('board_member_upload'))
        board_image=board_members_upload(file)
        data = Boardmembers(name=name,position=position,description="none",fileurl=board_image)
        db.session.add(data)
        db.session.commit()
        flash('New Board Member Added')
        return redirect(url_for('board_member_upload'))
    items=Boardmembers.query.all()
    return render_template("backend/add-boardmember.html",items=items)
@app.route("/admin/addseniormanagement", methods=['POST','GET'])
@login_required
def senior_management_upload():
    if request.method == 'POST':
        name=request.form['name']
        position=request.form['position']
        
        if 'management_image' not in request.files:
            flash('No file part')
            return redirect(url_for('board_member_upload'))
        file = request.files['management_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('board_member_upload'))
        board_image=seniormanagement_upload(file)
        data = Seniormanagement(name=name,position=position,description="none",fileurl=board_image)
        db.session.add(data)
        db.session.commit()
        flash('New Senior Manager  Added')
        return redirect(url_for('senior_management_upload'))
    items=Seniormanagement.query.all()
    return render_template("backend/add-seniormanagement.html",items=items)
@app.route("/boardmember/delete/<int:id>", methods=['POST','GET'])
@login_required
def boardmember_delete(id):
    Boardmembers.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Board Member Removed')
    return redirect(url_for('board_member_upload'))
@app.route("/seniormanagement/delete/<int:id>", methods=['POST','GET'])
@login_required
def seniormanagement_delete(id):
    Seniormanagement.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Senior Manager Removed')
    return redirect(url_for('senior_management_upload'))
@app.route("/admin/addnews", methods=['POST','GET'])
@login_required
def news_upload():
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        if 'news_image' not in request.files:
            flash('No file part')
            return redirect(url_for('news_upload'))
        file = request.files['news_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('news_upload'))
        news_image=upload_picture(file)
        data = News(title=title,description=description,fileurl=news_image)
        db.session.add(data)
        db.session.commit()
        flash('Submitted')
        return redirect(url_for('news_upload'))
    items=News.query.all()
    return render_template("backend/add-news.html",items=items)
@app.route("/news/delete/<int:id>", methods=['POST','GET'])
@login_required
def news_delete(id):
    News.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    return redirect(url_for('news_upload'))

@app.route(f"{baseUrl}")
@login_required
def dashboard():
    return render_template("backend/dashboard.html")

@app.route("/licensestatus/<int:id>", methods=['POST','GET'])
@login_required
def licensestatus(id):
    id=id
    if request.method == 'POST':
        id=id
        status=request.form['status']
        Licence.query.filter_by(id=id).update({'status':status})
        db.session.commit()
        return redirect(url_for('admin_licenses'))

    return render_template("backend/license_view.html")


@app.route(f"{baseUrl}/edit/board/<int:id>", methods=['POST'])
@login_required
def board_edit(id):
    if request.method == 'POST':
        boardmember = Boardmembers.query.filter_by(id=id).first()
        name=request.form['name']
        position=request.form['position']
          
        file = request.files['board_image']
        
        if file.filename == '':
            boardmember.fileurl=boardmember.fileurl
        else:
            board_image=board_members_upload(file)
            boardmember.fileurl=board_image
           
        
        boardmember.name = name
        boardmember.position = position
        db.session.add(boardmember)
        db.session.commit()
        flash('Boardmember updated')
        return redirect(url_for('board_member_upload'))

@app.route(f"{baseUrl}/edit/seniormanagement/<int:id>", methods=['POST'])
@login_required
def senior_edit(id):
    if request.method == 'POST':
        seniormanager = Seniormanagement.query.filter_by(id=id).first()
        name=request.form['name']
        position=request.form['position']
          
        file = request.files['board_image']
        
        if file.filename == '':
            seniormanager.fileurl=seniormanager.fileurl
        else:
            board_image=seniormanagement_upload(file)
            seniormanager.fileurl=board_image
           
        
        seniormanager.name = name
        seniormanager.position = position
        db.session.add(seniormanager)
        db.session.commit()
        flash('Senior Manager updated')
        return redirect(url_for('senior_management_upload'))
@app.route(f"{baseUrl}/edit/career/<int:id>", methods=['POST'])
@login_required
def career_edit(id):
    if request.method == 'POST':
        career = Careers.query.filter_by(id=id).first()
        title=request.form['career_title']
        career_description=request.form['career_description']
        expiry_date=request.form['duedate']  
        file = request.files['career_pdf']
        
        if file.filename == '':
            career.career_fileurl=career.career_fileurl
        else:
            career_pdf=upload_career_file(file)
            career.career_fileurl=career_pdf
           
        
        career.title= title
        career.career_description=career_description
        career.expiry_date=expiry_date
        
        db.session.add(career)
        db.session.commit()
        flash('Career updated')
        return redirect(url_for('career_upload'))
@app.route(f"{baseUrl}/edit/tenders/<int:id>", methods=['POST'])
@login_required
def tender_edit(id):
    if request.method == 'POST':
        tender = Tenders.query.filter_by(id=id).first()
        ref_number=request.form['ref']
        title=request.form['title']
        department=request.form['department']
        
        upload_date=request.form['uploaddate']
        due_date=request.form['duedate']
        file=request.files['pdf_url']
        
        
        if file.filename == '':
            tender.file_url=tender.file_url
        else:
            pdf_url=tenders_uploads(file)
            tender.file_url=pdf_url
           
        
        tender.ref_number=ref_number
        tender.title=title
        tender.department=department
        tender.upload_date=upload_date
        tender.due_date=due_date

        
        db.session.add(tender)
        db.session.commit()
        flash('Tender updated')
        return redirect(url_for('tender_upload'))

@app.route(f"{baseUrl}/edit/item/<int:id>", methods=['POST'])
@login_required
def whatwedoitem_edit(id):
    if request.method == 'POST':
        whatwedoitem = Item_model.query.filter_by(id=id).first()
        cover_page_title = request.form['cover_page_title']
        item_name = request.form['item_name']
        sort_order = request.form['sort_order'] #NEW ADDED FIELD
        item_route_name = request.form['item_route_name'] #NEW ADDED FIELD
        cover_page_description = request.form['cover_page_description']
        body_text = request.form['body_text']
        sort_order = request.form['sort_order'] #NEW ADDED FIELD

       

    
            
            
        file = request.files['body_image']
        if file.filename == '':
            whatwedoitem.body_image=whatwedoitem.body_image
        else:
            body_image=whatwedo_upload(file)
            whatwedoitem.body_image=body_image
            
            
        file = request.files['cover_image']
        if file.filename == '':
            whatwedoitem.cover_image=whatwedoitem.cover_image
        else:
            new_cover_image=whatwedo_upload(file)
            whatwedoitem.cover_image=new_cover_image
            print("new_cover_image")

        
        file = request.files['pdf']
        if file.filename == '':
            whatwedoitem.pdf=whatwedoitem.pdf
        else:
            pdf_file=whatwe_do_pdf(file)
            whatwedoitem.pdf=pdf_file
        if body_text == '':
            whatwedoitem.body_text =whatwedoitem.body_text
            
        else:
            whatwedoitem.body_text = body_text
        
        
        if cover_page_title == '':
            whatwedoitem.cover_page_title = whatwedoitem.cover_page_title
        else:
            whatwedoitem.cover_page_title = cover_page_title
        if cover_page_description == '':
            whatwedoitem.cover_page_description = whatwedoitem.cover_page_description
        else:
            whatwedoitem.cover_page_description = cover_page_description
        if item_name == '':
            whatwedoitem.item_name = whatwedoitem.item_name
        else:
            whatwedoitem.item_name = item_name
        if sort_order == '':
            whatwedoitem.sort_order = whatwedoitem.sort_order
        else:
            whatwedoitem.sort_order = sort_order
        if item_route_name == '':
            whatwedoitem.item_route_name = whatwedoitem.item_route_name
        else:
            whatwedoitem.item_route_name = item_route_name

        
        
        
       
      
    

          
        db.session.add(whatwedoitem)
        db.session.commit()
        flash('Item updated')
        return redirect(url_for('admin_item',id=1))
        
@app.route(f"{baseUrl}/upload-project", methods=['POST','GET'])
@login_required
def project_uploads():
    
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        if 'project_image' not in request.files:
            flash('No file part')
            return redirect(url_for('gallery_uploads'))
        file = request.files['project_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('gallery_uploads'))
        project_image=project_upload(file)
        data = Projects(file_url=project_image, title=title,descriptions=description)
        db.session.add(data)
        db.session.commit()
        flash('Project Image Uploaded')
        return redirect(url_for('project_uploads'))
    items=Projects.query.all()
    return render_template("backend/projectsupload.html",items=items)
@app.route("/project/delete/<int:id>", methods=['POST','GET'])
@login_required
def project_delete(id):
    Projects.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Projects Image Deleted')

    return redirect(url_for('project_uploads'))

@app.route(f"{baseUrl}/regionaloffices", methods=['POST','GET'])
@login_required
def regionaloffice():

    if request.method == 'POST':
        title = request.form['title']
        description = request.form['description']
        if 'office_image' not in request.files:
            flash('No file part')
            
        file = request.files['office_image']
        if file.filename == '':
            flash('No selected file')
            
        office_image=regionaloffices_uploads(file)
        
        print(office_image)
        if office_image==None:
           office_image=''
        if 'cover_image' not in request.files:
            flash('No file part')
            
        file = request.files['cover_image']
        if file.filename == '':
            flash('No selected file')
            
        cover_image=regionaloffices_uploads(file)
        
        print(cover_image)
        if cover_image==None:
           cover_image=''
        if 'pdf' not in request.files:
            flash('No file part')
            print('No file part')
            
        file = request.files['pdf']
        if file.filename == '':
            
            print('No pdf file part')
        
        pdf_file=regionaloffices_pdf(file)
        print("pdf")
        print(pdf_file)
        data = Offices(image_url=office_image, pdf_url=pdf_file, title=title, descriptions=description,cover_image_url=cover_image)
        db.session.add(data)
        db.session.commit()
        flash('New office added')
        return redirect(url_for('regionaloffice'))
    items=Offices.query.all()
    return render_template("backend/regional_offices_upload.html",items=items)
@app.route("/regionaloffices/delete/<int:id>", methods=['POST','GET'])
@login_required
def office_delete(id):
    Offices.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    return redirect(url_for('regionaloffice'))
@app.route(f"{baseUrl}/uploadpartner", methods=['POST','GET'])
@login_required
def partner_uploads():
    
    if request.method == 'POST':
        title=request.form['title']
        link=request.form['link']
        if 'partner_image' not in request.files:
            flash('No file part')
            return redirect(url_for('partner_uploads'))
        file = request.files['partner_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('partner_uploads'))
        partners_image=partners_upload(file)
        data = Partners(file_url=partners_image, title=title,link=link)
        db.session.add(data)
        db.session.commit()
        flash('Partner Image Uploaded')
        return redirect(url_for('partner_uploads'))
    items=Partners.query.all()
    return render_template("backend/partners_upload.html",items=items)

@app.route("/partner/delete/<int:id>", methods=['POST','GET'])
@login_required
def partner_delete(id):
    Partners.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Partners Image Deleted')

    return redirect(url_for('partner_uploads'))

@app.route(f"{baseUrl}/about", methods=['POST','GET'])
@login_required
def about_uploads():
    
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        sort=request.form['sort_order']
        if 'about_image' not in request.files:
            flash('No file part')
            return redirect(url_for('about_uploads'))
        file = request.files['about_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('about_uploads'))
        gallery_image=about_upload(file)
        data = About(file_url=gallery_image, title=title,descriptions=description,sort_order=sort)
        db.session.add(data)
        db.session.commit()
        flash('About Image Uploaded')
        return redirect(url_for('about_uploads'))
    items=About.query.all()
    return render_template("backend/about_upload.html",items=items)

@app.route("/about/delete/<int:id>", methods=['POST','GET'])
@login_required
def about_delete(id):
    About.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('About Image Deleted')

    return redirect(url_for('about_uploads'))
@app.route(f"{baseUrl}/editindex", methods=['POST','GET'])
@login_required
def edit_index():
    item=Index.query.first()
    if request.method == 'POST':
        
        image=request.form['image_url']
        video=request.form['video_url']
        what_we_do_one=request.form['one']
        what_we_do_two=request.form['two']
        what_we_do_three=request.form['three']
        what_we_do_four=request.form['four']
        what_we_do_five=request.form['five'] 
        item.image_url=image
        item.video_url=video    
        item.what_we_do_one=what_we_do_one
        item.what_we_do_two=what_we_do_two
        item.what_we_do_three=what_we_do_three
        item.what_we_do_four=what_we_do_four
        item.what_we_do_five=what_we_do_five
        db.session.add(item)
        db.session.commit()
        flash('Index page INFO updated')
        return redirect(url_for('edit_index'))
    
    return render_template("backend/indexupload.html",item=item)
@app.route(f"{baseUrl}/regionaloffices/edit/<int:id>", methods=['POST','GET'])
@login_required
def edit_regionaloffice(id):
    item=Offices.query.filter_by(id=id).first()
    if request.method == 'POST':
        title=request.form['title']
        file = request.files['office_image']
        body_text=request.form['description']
        item.descriptions=body_text
        if file.filename == '':
            item.image_url =item.image_url 
        else:
            office_image=regionaloffices_uploads(file)
            item.image_url=office_image


        file = request.files['cover_image']
        if file.filename == '':
            item.cover_image_url =item.cover_image_url
        else:
            cover_image=regionaloffices_uploads(file)
            item.cover_image_url=cover_image
        file = request.files['pdf']
        if file.filename == '':
            item.pdf_url =item.pdf_url
            print(item.pdf_url)
        else:
            pdf_file=regionaloffices_pdf(file)
            item.pdf_url=pdf_file
        
        item.title=title
        
        db.session.add(item)
        db.session.commit()
        flash('Regional Office Details Uploaded')
        return redirect(url_for('regionaloffice'))

    return render_template("backend/regionaloffice_upload.html",item=item)

@app.route(f"{baseUrl}/uploadpartner/edit/<int:id>", methods=['POST','GET'])
@login_required   
def edit_partner(id):
    item=Partners.query.filter_by(id=id).first()
    if request.method == 'POST':
        title=request.form['title']
        link=request.form['link']
        file = request.files['partner_image']
        if file.filename == '':
            item.file_url =item.file_url
        else:
            partners_image=partners_upload(file)
            item.file_url=partners_image
        item.title=title
        item.link=link
        db.session.add(item)
        db.session.commit()
        flash('Partner detail updated')
        return redirect(url_for('partner_uploads'))

    return render_template("backend/partners_upload.html",item=item)
@app.route(f"{baseUrl}/about/edit/<int:id>", methods=['POST','GET'])
@login_required   
def edit_about(id):
    item=About.query.filter_by(id=id).first()
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        sort=request.form['sort_order']
        file = request.files['about_image']
        if file.filename == '':
            item.file_url =item.file_url
        else:
            about_image=about_upload(file)
            item.file_url=about_image
        item.title=title
        item.descriptions=description
        item.sort_order=sort
        db.session.add(item)
        db.session.commit()
        flash('About detail updated')
        return redirect(url_for('about_uploads'))
    return render_template("backend/about_upload.html",item=item)
@app.route(f"{baseUrl}/upload-project/edit/<int:id>", methods=['POST','GET'])
@login_required   
def edit_project(id):
    item=Projects.query.filter_by(id=id).first()
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
       
        file = request.files['project_image']
        if description == '':
            item.descriptions =item.descriptions
        else:
            item.descriptions=description
        if file.filename == '':
            item.file_url =item.file_url
        else:
            project_image=project_upload(file)
            item.file_url=project_image
        item.title=title
        
        
        db.session.add(item)
        db.session.commit()
        flash('Project detail updated')
        return redirect(url_for('project_uploads'))
 
@app.route(f"{baseUrl}/edit/newsitem/<int:id>", methods=['POST'])
@login_required
def news_edit(id):
    if request.method == 'POST':
        newsitem = Item_model.query.filter_by(id=id).first()
        #NEW ADDED FIELD
        title=request.form['title']
        description=request.form['description']
        print(description)
        item_route_name = request.form['item_route_name']
        file = request.files['news_image']
        if file.filename == '':
            newsitem.body_image =newsitem.body_image
        else:
            news_image=news_uploads(file)
            newsitem.body_image=news_image
        file = request.files['pdf']
        if file.filename == '':
            newsitem.pdf =newsitem.pdf
        else:
            pdf_file=news_pdf(file)
            newsitem.pdf=pdf_file
        newsitem.cover_page_title=title
        newsitem.cover_page_description=description
        newsitem.item_route_name=item_route_name
        db.session.add(newsitem)
        db.session.commit()
        flash('News item updated')
        return redirect(url_for('admin_item',id=2))

@app.route(f"{baseUrl}/upload-gallery/edit/<int:id>", methods=['POST','GET'])
@login_required
def gallery_edit(id):
    item=Gallery_model.query.filter_by(id=id).first()
    
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
    
        file = request.files['gallery_image']
        if file.filename == '':      
            item.file_url =item.file_url
        else:
            gallery_image=gallery_upload(file)
            item.file_url=gallery_image
        item.title=title
        item.descriptions=description
        db.session.add(item)
        db.session.commit()
        flash('Gallery item updated')
        return redirect(url_for('gallery_uploads'))
        
    items=Gallery_model.query.all()
    return render_template("backend/galleryupload.html",items=items)
       
@app.route(f"{baseUrl}/editcharter", methods=['POST','GET'])
@login_required
def edit_charter():
    item=Charter.query.filter_by(id=1).first()
    if request.method == 'POST':
        file=request.files['englishp']
        if file.filename == '':
            item.english_pdf =item.english_pdf
        else:
            charter_pdfs=charter_pdf(file)
            item.english_pdf=charter_pdfs
        file=request.files['kiswahilip']
        if file.filename == '':
            item.kiswahili_pdf =item.kiswahili_pdf
        else:
            charter_pdfs=charter_pdf(file)
            item.kiswahili_pdf=charter_pdfs
        file=request.files['englisha']
        if file.filename == '':
            item.english_audio =item.english_audio
        else:
            charter_audios=charter_audio(file)
            item.english_audio=charter_audios
        file=request.files['kiswahilia']
        if file.filename == '':
            item.kiswahili_audio =item.kiswahili_audio
        else:
            charter_audios=charter_audio(file)
            item.kiswahili_audio=charter_audios
        db.session.add(item)
        db.session.commit()
    return render_template("backend/servicecharter.html",item=item)
    
@app.route(f"{baseUrl}/upload-innovations", methods=['POST','GET'])
@login_required
def innovation_uploads():
    
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        cover_description=request.form['cover_description']
      
        file = request.files['project_image']
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('innovation_uploads'))
        project_image=innovation_upload(file)
        file = request.files['project_pdf']
        if file.filename == '':
            flash('No selected file')
            project_pdf=''
        else:
            project_pdf=innovation_pdf(file)
        data = Innovation(file_url=project_image, title=title,descriptions=description,pdf_url=project_pdf,coverpage_description=cover_description)
        db.session.add(data)
        db.session.commit()
        flash('Project Image Uploaded')
        return redirect(url_for('innovation_uploads'))
    items=Innovation.query.all()
    return render_template("backend/innovationsupload.html",items=items)       
            

@app.route(f"{baseUrl}/upload-innovations/edit/<int:id>", methods=['POST','GET'])
@login_required   
def edit_innovation(id):
    item=Innovation.query.filter_by(id=id).first()
    if request.method == 'POST':
        title=request.form['title']
        description=request.form['description']
        cover_description=request.form['cover_description']
       
        file = request.files['project_image']
        if cover_description == '':
            item.coverpage_description =item.coverpage_description
        else:
            item.coverpage_description=cover_description
        if description == '':
            item.descriptions =item.descriptions
        else:
            item.descriptions=description
        if file.filename == '':
            item.file_url =item.file_url
        else:
            project_image=innovation_upload(file)
            item.file_url=project_image
        file = request.files['project_pdf']
        if file.filename == '':
            item.pdf_url =item.pdf_url
        else:
            project_image=innovation_pdf(file)
            item.pdf_url=project_image
        item.title=title
        
        
        db.session.add(item)
        db.session.commit()
        flash('Project detail updated')
        return redirect(url_for('innovation_uploads'))
@app.route("/innovation/delete/<int:id>", methods=['POST','GET'])
@login_required
def innovation_delete(id):
    Innovation.query.filter_by(id=id).delete()
    # category = Category_model.query.filter_by(id=categoryID).first()
    db.session.commit()
    flash('Innovation Image Deleted')

    return redirect(url_for('innovation_uploads'))
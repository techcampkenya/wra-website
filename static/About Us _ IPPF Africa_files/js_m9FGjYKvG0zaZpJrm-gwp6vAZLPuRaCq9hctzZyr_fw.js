(function () {

    // beging langcode detection
    let paths = location.pathname.split('/')
    paths = paths.filter(function (e) { return e !== '' }) // remove empty element
    let langcode = (paths.length > 0 && paths[0].length === 2) ? '/' + paths[0] : '';
    // end langcode detection

    let windowWidth = document.body.clientWidth;

    var host = window.location.protocol + "//" + window.location.host + langcode;

    var regionsMembersWrapper = document.querySelector('#regions-members')

    var associationsWrapper = document.querySelector('.associations-wrapper')
    var allRegionMembers = document.querySelectorAll('.region-member')

    var associationDescription = document.querySelector('.association-description')
    var associationList = document.querySelector('.region-association-list')

    var closeButton = document.querySelector('.close-list-container')

    let swiper;

    const setWrapperHeight = () => {

        var isDescriptionActive = associationDescription.classList.contains('active')

        associationsWrapper.style.minHeight = `${1450}px`
        regionsMembersWrapper.style.minHeight = `${1450}px`

        if(isDescriptionActive){
            associationsWrapper.style.minHeight = `${2100}px`
            regionsMembersWrapper.style.minHeight = `${2100}px`
        }
    }


    const disablePointerEvents = (value) => {
        const globeContainer = document.querySelector('.globe-container')
        const membersContainer = document.querySelector('.members-container')

        globeContainer.style.pointerEvents = "all";
        membersContainer.style.pointerEvents = "all";

        if(value){
            globeContainer.style.pointerEvents = "none";
            membersContainer.style.pointerEvents = "none";
        }
    }

    associationsWrapper.addEventListener('click', () => {
        associationList.classList.remove('active')
        associationDescription.classList.remove('active')
        closeButton.classList.remove('active')

        disablePointerEvents(false)


        allRegionMembers.forEach((element, index) => {
            element.classList.remove('active')
        })

        associationsWrapper.style.minHeight = `unset`
        regionsMembersWrapper.style.minHeight = `unset`

        regionsMembersWrapper.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    })


    allRegionMembers.forEach((element, index) => {

        element.addEventListener('click', (e) => {
            let memberListEndPoint = `${host}/v1/list-region-members/${element.id}?_format=json`;
            let getMemberListData = fetch(memberListEndPoint)
                .then((response) => {
                    return response.json();
                })
                .then(result => {

                    allRegionMembers.forEach((element, index) => {
                        element.classList.remove('active')
                    })
                    e.target.classList.add('active')

                    disablePointerEvents(true)


                    swiper.removeAllSlides()

                    result.forEach((element, index) => {
                        swiper.appendSlide(
                            `
                            <div id="${element.nid}" class="swiper-slide">
                                <div class="association-slide">
                                    <h3>${element.title}</h3>
                                    <span>${element.field_region}</span>
                                </div>
                            </div>

                            `
                        );
                    })

                    associationList.classList.add('active')
                    closeButton.classList.add('active')
                    regionsMembersWrapper.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});

                    setWrapperHeight()

                    swiper.slides.forEach((slide, index) => {

                        slide.addEventListener('click', (e) => {
                            let allSlides = document.querySelectorAll('.association-slide');


                            let prevSelectedSlide = document.querySelector('.association-slide.active')

                            allSlides[index].classList.add('active')

                            if(prevSelectedSlide !== null){
                                prevSelectedSlide.classList.remove('active')
                                allSlides[index].classList.add('active')
                            }


                            let memberDataEndPoint = `${host}/v1/member-info/${slide.id}?_format=json`;
                            let getMemberData = fetch(memberDataEndPoint)
                                .then((response) => response.json())
                                .catch(error => {
                                    console.log(error);
                                })

                            Promise.race([getMemberData]).then(result => {
                                associationDescription.classList.add('active')

                                let description = `
                                    <div>
                                        <h3>${result[0].title}</h3>

                                        <p>${result[0].body}</p>

                                        <div class="read-more-dark-button">
                                            ${result[0].view_node}
                                        </div>
                                    </div>

                                    <div class="association-logo">
                                        ${result[0].field_image}
                                    </div>

                                `
                                associationDescription.innerHTML = description

                                if(windowWidth < 1300){
                                    regionsMembersWrapper.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});

                                }
                                setWrapperHeight()

                            }).catch(error => console.error(error));
                        })
                    })
                //
                })
                .catch(error => {
                    console.log(error);
                })
        });
    })

    const initSlider = (sliderId, nextId, prevId) => {

        swiper = new Swiper(`#${sliderId}`, {
            direction: "vertical",
            slidesPerView: 'auto',
            slidesPerGroup: 1,
            spaceBetween: 10,
            mousewheel: true,
            freeMode: false,
            loop: false,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: `#${nextId}`,
                prevEl: `#${prevId}`,
            },
            on: {

                init: {},
                slideNextTransitionStart: {},
                slidePrevTransitionStart: {},
            },
        });

    }

    const sliders = document.querySelectorAll(".region-association-carousel");

    sliders.forEach((element, index) => {

        const slider = element.children[0];
        const sliderId = `swiper-slider-${uuidv4()}`
        slider.id = sliderId

        const prev = element.children[1];
        const prevId = `swiper-button-prev-${uuidv4()}`
        prev.id = prevId

        const next = element.children[2];
        const nextId = `swiper-button-next-${uuidv4()}`
        next.id = nextId


        initSlider(sliderId, nextId, prevId)
    })
})()
;

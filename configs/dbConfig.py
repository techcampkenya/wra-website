import os

from flask_sqlalchemy import SQLAlchemy
from main import app
db = SQLAlchemy(app)

###########################TESTING  DB CONFIG########################################
class TestingConfig:

    SQLALCHEMY_DATABASE_URI = "sqlite:///wrasite.db"
    # SQLALCHEMY_DATABASE_URI='postgresql://wra_test:Wra_2022@172.17.0.1:5432/wra_test'
    SECRET_KEY = os.urandom(20)
    JWT_SECRET_KEY = os.urandom(20)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False

   ###########################DEVELOPMENT DB CONFIG########################################
# class DevelopmentConfig:
#     JWT_SECRET_KEY= os.urandom(20)
#     SQLALCHEMY_DATABASE_URI = "postgresql://kttrfccczmefjj:c6de48a7d488e3db747fd541a52bec236dc6dc17566031732620166258dad396@ec2-99-80-170-190.eu-west-1.compute.amazonaws.com:5432/d3h704goo9vrnj"
#     SECRET_KEY = os.urandom(20)
#     SQLALCHEMY_TRACK_MODIFICATIONS = "SQLALCHEMY_TRACK_MODIFICATIONS"
#     DEBUG = "DEBUG"
#     FLASK_APP = "FLASK_APP"
#     FLASK_ENV = "FLASK_ENV"

from turtle import title
import math
from flask import render_template,request,redirect,url_for,flash
from main import app
from models.about_us import About
from models.boardmembers import Boardmembers
from models.category import Category_model, Item_model, License_model
from models.downloads import Downloads_model
from configs.dbConfig import TestingConfig,db
from models.contact_us import Contact_model
from models.gallery import Gallery_model
from models.licence import Licence
from models.news import News
from models.tenders import Tenders
from models.boardmembers import Boardmembers
from models.seniormanagement import Seniormanagement
import scrapetube
import json
from models.careers import Careers
from models.projects import Projects
from models.regionaloffices import Offices
from models.partners import Partners
from models.index import Index
from models.charter import Charter
from models.innovations import Innovation
import datetime

app.config.from_object(TestingConfig)

@app.template_filter()
def format_datetime(value, format='medium'):
    # Create a python date object to work on
    date_obj = datetime.datetime.strptime(value, '%Y-%m-%d')
    if format == 'full':
        return date_obj.strftime("%m/%d/%Y")
    elif format == 'medium':
        return date_obj.strftime("%d-%b-%Y")


## Frontend Routes
@app.route("/home-two")
def home_two():
    return render_template("frontend/index.html")


@app.route("/")
def home():
    categoryId =  Category_model.query.filter_by(category_route_name='what-we-do').first().id
    items =  Item_model.query.filter_by(category_id=categoryId).order_by(Item_model.sort_order.asc()).all()
    index=Index.query.first()
    return render_template("frontend/index2.html",items=items[:4],index=index)

@app.route("/about-us")
def about():
    items=About.query.all()
    return render_template("frontend/about3.html" ,items=items)

@app.route("/about-test")
def abouttest():
     return render_template("frontend/navchanger.html")

@app.route("/projects")
def projects():
    items= Projects.query.order_by(Projects.id.asc()).all()
    icons=Partners.query.all()
    return render_template("frontend/projects.html",items=items,icons=icons)

@app.route("/tenders")
def tenders():
    tenders=Tenders.query.all()
    return render_template("frontend/tender_new.html",tenders=tenders)

@app.route("/media")
def media():
    data=Gallery_model.query.all()
    
    return render_template("frontend/media.html", data=data)

@app.route("/careers")
def careers():
    careers=Careers.query.all()
    due_date=[]
    status=[]
    now=datetime.datetime.now()
    # now=now.strftime("%Y-%m-%d %H:%M")
    def checkdate(date):
        x = date.replace("T", " ")
        date_time_obj = datetime.datetime.strptime(x, '%Y-%m-%d %H:%M')
        
        x=date_time_obj<datetime.datetime.now()
        print(x)
        if x==True:

            return 1
        else:
            return 0
    # for item in items:
    #     x = item.expiry_date.replace("T", " ")
    #     date_time_obj = datetime.datetime.strptime(x, '%Y-%m-%d %H:%M')
    #     print('Date:', date_time_obj.date())
    #     due_date.append(date_time_obj.date())
    #     x=date_time_obj>datetime.datetime.now()
    #     print(x)
    #     status.append(x)
    #     print(type(x))
    return render_template("frontend/careers.html", careers=careers,checkdate=checkdate)


@app.route("/publications")
def publications():
    downloadables = Downloads_model.query.order_by(Downloads_model.id.desc()).all()
    return render_template("frontend/publications.html",downloadables=downloadables)

@app.route("/wra-news",methods=['GET', 'POST'])
def downloads(): 
    if request.method=='POST':
        page_number= int(request.form["page"])
        even_page_number = page_number if page_number%2 == 0 else page_number+1
        print(even_page_number)

        #     items= Projects.query.order_by(Item_model.id.desc()).all()
        category_id = Category_model.query.filter_by(category_route_name='wra-news').first().id
        items =  Item_model.query.filter_by(category_id=category_id).order_by(Item_model.id.desc()).all()
        print(';-;'*20,items)
        items_ = items[(page_number*4)-4:(page_number*4)]
        paginator_counter = math.ceil(len(items) / 4)
        return render_template("frontend/news5.html",items=items_,paginator_counter=paginator_counter)

    category_id = Category_model.query.filter_by(category_route_name='wra-news').first().id
    items =  Item_model.query.filter_by(category_id=category_id).order_by(Item_model.created_at.desc()).all()
 
    items_ = items[:4]
  
  
    print('-'*20)
    paginator_counter = math.ceil(len(items) / 4)
    return render_template("frontend/news5.html",items=items_,paginator_counter=paginator_counter)

@app.route("/wra-news/<string:item_route_name>")
def generic(item_route_name):
    item = Item_model.query.filter_by(item_route_name=item_route_name).first()

    category_id = Category_model.query.filter_by(category_route_name='wra-news').first().id
    relatedLinks =  Item_model.query.filter_by(category_id=category_id).all()
    # for i in items:
    #     print(i.item_route_name)
    print(item)
    title = item.cover_page_title
    description = item.cover_page_description
    image = item.body_image
    body_text = item.body_text
    print(body_text)
    created_at = item.created_at
    pdf=item.pdf
    
    return render_template("frontend/genericnews.html",item=item,title=title,description=description,image=image,body_text=body_text,created_at=created_at,pdf=pdf,relatedLinks=relatedLinks[:10])

# @app.route("/downloads")
# def downloads():
#     return render_template("frontend/downloads.html")

@app.route("/what-we-do")
def whatwedo():
    categoryId =  Category_model.query.filter_by(category_route_name='what-we-do').first().id
    items =  Item_model.query.filter_by(category_id=categoryId).order_by(Item_model.sort_order.asc()).all()
    
    return render_template("frontend/whatwedo2.html",items=items)
    
@app.route("/what-we-do/<string:item_route_name>")
def what_we_do_generic(item_route_name):
    item = Item_model.query.filter_by(item_route_name=item_route_name).first()
    print(item)
    pdf=item.pdf
    item_name = item.item_name
    cover_page_title = item.cover_page_title
    cover_page_description = item.cover_page_description
    body_image = item.body_image
    body_text = item.body_text
    created_at = item.created_at
    cover_image = item.cover_image

    category_id = Category_model.query.filter_by(category_route_name='what-we-do').first().id
    relatedLinks = Item_model.query.filter_by(category_id=category_id).all()
    print('-'*100)
    print(relatedLinks)
    print('-'*100)

    return render_template("frontend/generic.html",item_name=item_name,cover_page_title=cover_page_title,
    cover_page_description=cover_page_description,body_image=body_image,
    body_text=body_text,created_at=created_at,pdf=pdf,cover_image=cover_image, relatedLinks=relatedLinks)


@app.route("/board-members")
def boardmembers():
    items=Boardmembers.query.all()
    return render_template("frontend/boardmember.html",items=items)

@app.route("/top-management")
def topmanagement():
    items=Seniormanagement.query.all()
    return render_template("frontend/topmanagement.html",items=items)

@app.route("/regional-offices")
def regionaloffices():
    

    return render_template("frontend/regional-offices-two.html", items = Offices.query.all())

@app.route("/regional-offices/<string:routeName>")
def singleRegionaloffices(routeName):
    
    office = Offices.query.filter_by(title=routeName).first()
    print(office.title)
    relatedLinks = Offices.query.all()
    
    return render_template("frontend/generic-regional-offices.html",item_name=office.title,cover_page_title=office.title,
    cover_page_description=office.descriptions[:120],body_image=office.image_url,
    body_text=office.descriptions,created_at="",pdf=office.pdf_url,cover_image=office.image_url, relatedLinks=relatedLinks)

@app.route("/service-charter")
def serviceCharter():
    item=Charter.query.first()
    return render_template("frontend/serviceCharter.html",item=item)
    
@app.route("/feedback")
def feedback():
    return render_template("frontend/feedback.html")

@app.route("/videos")
def videos():
    videos = scrapetube.get_channel("UC8l05WDkeTu3kepsJUmfdrg")
    print(videos)
    print("hi")
    videosList =[]
    vidcount= 1

    for video in videos:
        print
        videosList.append({"name": video['title']["runs"][0]['text'], "data": video['videoId']})
        vidcount = vidcount + 1

    videosList = json.dumps(videosList)     

    return render_template("frontend/videos.html", videosList=videosList)


@app.route("/contact-us",methods=['GET', 'POST'])
def contact():
    if request.method=='POST':
        fname =  request.form["fname"]
        email =  request.form["email"]
        message =  request.form["message"]
        
        row = Contact_model(full_name = fname , email = email, message = message)
        db.session.add(row)
        db.session.commit()
    
    return render_template("frontend/contact.html")


@app.route("/laboratory-services")
def laboratoryServices():
    return render_template("frontend/laboratoryServices.html")

@app.route("/surface-water")
def surfacewater():
    return render_template("frontend/surfacewater.html")

@app.route("/ground-water")
def groundwater():
    return render_template("frontend/groundwater.html")

@app.route("/water-quality")
def waterquality():
    return render_template("frontend/waterquality.html")

@app.route("/permitting")
def permitting():
    return render_template("frontend/permitting.html")


@app.route("/water-resources-data")
def waterresourcesdata():
    return render_template("frontend/waterresourcesdata.html")

@app.route("/community-development")
def communitydevelopment():
    return render_template("frontend/community-development.html")

@app.route("/access-to-info")
def accessToInfo():
    return render_template("frontend/accessToInfo.html")

@app.route("/licenses",methods=['GET', 'POST'])
def licenses():
    if request.method=='POST':
        refno =  request.form["refno"]
        email =  request.form["email"]
        idnumber =  request.form["idnumber"]
        license = Licence(ref_number = refno, id_number=idnumber, email=email,status="Not Approved")
        db.session.add(license)
        db.session.commit()
        flash('Application recieved, we will get back to you via email.')
        return redirect(url_for('licenses'))
    return render_template("frontend/license.html")


@app.route("/projects/<string:routename>")
def projects_generic(routename):
    routename=routename.replace("_"," ")
    relatedLinks=Projects.query.all()
    print(relatedLinks)
    item = Projects.query.filter_by(title=routename).first()
    return render_template("frontend/projects_generic.html",item=item,relatedLinks=relatedLinks)


@app.route("/report-corruption",methods=['GET', 'POST'])
def corruption():
    if request.method=='POST':
        # refno =  request.form[""]
        # email =  request.form[""]
        # idnumber =  request.form[""]
        #Add corruption model here 
        
        flash('Report Successfully Recieved. We thank you for your feedback.')
        return redirect(url_for('corruption'))
    return render_template("frontend/corruption.html")
@app.route("/innovations",methods=['GET', 'POST'])
def innovation():
    items=Innovation.query.all()
    return render_template ("frontend/innovations.html",items=items)
@app.route("/innovations/<string:routename>")
def innovations_generic(routename):
    routename=routename.replace("_"," ")
    relatedLinks=Innovation.query.all()
    print(relatedLinks)
    item = Innovation.query.filter_by(title=routename).first()
    pdf_url=item.pdf_url
    print(pdf_url)
    return render_template("frontend/innovations_generic.html",item=item,relatedLinks=relatedLinks,pdf_url=pdf_url)
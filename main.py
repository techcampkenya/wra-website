from flask import Flask

app = Flask(__name__)


from frontendroutes import *
from backendroutes import *


if __name__ == '__main__':
    app.run(debug = True,port =5000)
